<?php
namespace Domi202\WraithPhp\Spider\Filter\Postfetch;

use VDB\Spider\Filter\PostFetchFilterInterface;
use VDB\Spider\Resource;
use Domi202\WraithPhp\Utility\StringUtility;

/**
 * Class MimeTypeFilter
 */
class MimeTypeFilter implements PostFetchFilterInterface
{
    /**
     * @var string
     */
    protected $allowedMimeType = '';

    /**
     * MimeTypeFilter constructor.
     * @param string $allowedMimeType
     */
    public function __construct($allowedMimeType)
    {
        $this->allowedMimeType = $allowedMimeType;
    }

    /**
     * @param Resource $resource
     * @return bool
     */
    public function match(Resource $resource)
    {
        $contentType = $resource->getResponse()->getHeaderLine('Content-Type');
        return !(StringUtility::isFirstPartOfStr($contentType, $this->allowedMimeType));
    }

    /**
     * @param string $str
     * @param string $partStr
     * @return bool
     */
    protected function isFirstPartOfStr($str, $partStr) {
        return $partStr != '' && strpos((string) $str, (string) $partStr, 0) === 0;
    }
}
