<?php
namespace Domi202\WraithPhp\Utility;

/**
 * Class FileUtility
 * @package Domi202\WraithPhp\Utility
 */
class FileUtility
{
    /**
     * @param string $value
     * @return string
     */
    public static function addTrailingSlash($value)
    {
        return rtrim($value, '/') . '/';
    }

    /**
     * @param string $directory
     * @return void
     */
    public static function emptyDirectory($directory)
    {
        if (!is_dir($directory)) {
            return;
        }
        foreach(new \DirectoryIterator($directory) as $fileInfo) {
            /* @var $fileInfo \DirectoryIterator */
            if ($fileInfo->isDot()) {
                continue;
            } elseif ($fileInfo->isDir()) {
                self::emptyDirectory($fileInfo->getRealPath());
                rmdir($fileInfo->getRealPath());
            } else {
                unlink($fileInfo->getRealPath());
            }
        }
    }

    /**
     * @param $bytes
     * @param int $dec
     * @return string
     */
    public static function formatFilesize($bytes, $dec = 2)
    {
        $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($bytes) - 1) / 3);

        return sprintf("%.{$dec}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

    /**
     * @param array $filePaths
     * @return bool
     */
    public static function filesExist(array $filePaths)
    {
        foreach ($filePaths as $key => $filePath) {
            if (!file_exists($filePath)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param string $directory
     * @return void
     */
    public static function prepareTempDirectory($directory)
    {
        $directory = self::addTrailingSlash($directory);
        if (!is_dir($directory)) {
            self::createDirectory($directory);
        } else {
            self::emptyDirectory($directory);
        }
    }

    /**
     * @param string $directory
     * @return void
     */
    static public function cleanUpTempDirectory($directory)
    {
        $directory = self::addTrailingSlash($directory);
        if (!is_dir($directory)) {
            self::emptyDirectory($directory);
            @rmdir($directory);
        }
    }

    /**
     * @param string $directory
     * @return bool
     */
    public static function createDirectory($directory)
    {
        if (is_dir($directory)) {
            return true;
        }
        return mkdir($directory, 0777, true);
    }

    /**
     * @param string $filePath
     * @param string $sourceFilePath
     * @return bool
     */
    public static function copyFileIfNotExists($filePath, $sourceFilePath)
    {
        return copy($sourceFilePath, $filePath);
    }

    /**
     * @param string $sourceDirectory
     * @param string $destinationDirectory
     * @return void
     */
    public static function copyBaseShots($sourceDirectory, $destinationDirectory)
    {
        if (!is_dir($sourceDirectory)) {
            return;
        }
        foreach(new \DirectoryIterator($sourceDirectory) as $fileInfo) {
            /* @var $fileInfo \DirectoryIterator */
            if ($fileInfo->isDot()) {
                continue;
            } elseif ($fileInfo->isDir()) {
                $nextLevelSource = self::addTrailingSlash($sourceDirectory) . $fileInfo->getFilename();
                $nextLevelDestination = self::addTrailingSlash($destinationDirectory) . $fileInfo->getFilename();
                self::createDirectory($nextLevelDestination);
                self::copyBaseShots($nextLevelSource, $nextLevelDestination);
            } else {
                $destinationFileName = $fileInfo->getBasename('.'.$fileInfo->getExtension()) . '_base.' . $fileInfo->getExtension();
                @copy(
                    self::addTrailingSlash($sourceDirectory) . $fileInfo->getFilename(),
                    self::addTrailingSlash($destinationDirectory) . $destinationFileName
                );
            }
        }
    }
}
