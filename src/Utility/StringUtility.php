<?php
namespace Domi202\WraithPhp\Utility;
use Domi202\WraithPhp\Exception;

/**
 * Class StringUtility
 * @package Domi202\WraithPhp\Utility
 */
class StringUtility
{
    /**
     * @param string $str
     * @param string $partStr
     * @return bool
     */
    public static function isFirstPartOfStr($str, $partStr) {
        return $partStr != '' && strpos((string) $str, (string) $partStr, 0) === 0;
    }

    /**
     * @param string $glue
     * @param string $subject
     * @return array
     * @throws Exception
     */
    public static function trimExplode($glue, $subject)
    {
        if (!is_string($subject)) {
            throw new Exception('$subject is not of type string');
        }
        $parts = explode($glue, $subject);
        foreach ($parts as &$part) {
            $part = trim($part);
        }
        return $parts;
    }
}
