<?php
namespace Domi202\WraithPhp\Utility;

/**
 * Class MathUtility
 * @package Domi202\WraithPhp\Utility
 */
class MathUtility
{
    /**
     * @param mixed $var
     * @return bool
     */
    public static function canBeInterpretedAsInteger($var)
    {
        if ($var === '' || is_object($var) || is_array($var)) {
            return false;
        }
        return (string)(int)$var === (string)$var;
    }

    /**
     * @param mixed $var
     * @return bool
     */
    public static function canBeInterpretedAsDouble($var)
    {
        if ($var === '' || is_object($var) || is_array($var)) {
            return false;
        }
        return (string)(double)$var === (string)$var;
    }

    /**
     * @param mixed $var
     * @return bool
     */
    public static function isExponentNumber($var)
    {
        $formattedNumber = number_format($var, 0, '', '');
        return self::canBeInterpretedAsInteger($formattedNumber);
    }
}
