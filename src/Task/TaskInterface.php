<?php
namespace Domi202\WraithPhp\Task;


interface TaskInterface
{
    /**
     * @return void
     */
    public function initialize();
}
