<?php
namespace Domi202\WraithPhp\Task;

use Domi202\WraithPhp\Domain\Model\Configuration;
use Domi202\WraithPhp\Task\Collection\TaskCollectionInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;


class TaskManager
{
    use ContainerAwareTrait;

    /**
     * @var TaskCollectionInterface
     */
    protected $taskCollection = array();

    /**
     * @var int
     */
    protected $finishedTasks;

    /**
     * @var int
     */
    protected $maxTasks;

    /**
     * @param TaskCollectionInterface $taskCollection
     * @param ContainerInterface $container
     * @return TaskManager
     */
    public function __construct(
        TaskCollectionInterface $taskCollection,
        ContainerInterface $container
    ) {
        $this->finishedTasks = 0;
        $this->taskCollection = $taskCollection;
        $this->setContainer($container);
        $this->maxTasks = $this->getConfiguration()->getMaxTasks();
    }

    /**
     * @return Configuration
     */
    protected function getConfiguration()
    {
        return $this->container->get('configuration');
    }

    /**
     * @return SymfonyStyle
     */
    protected function getStyle()
    {
        return $this->container->get('style');
    }

    /**
     * @return array
     */
    public function runTasksAsync()
    {
        // display progress bar
        $this->getStyle()->progressStart($this->taskCollection->count());

        while($this->taskCollection->hasRunningTasks()) {
            $this->step();
            usleep(100000);
        }

        // finish progress bar
        $this->getStyle()->progressFinish();

        $message = $this->taskCollection->countSuccessfulTasks() . ' / ' . $this->taskCollection->countExecutedTasks() . ' Tasks completed successfully';
        if ($this->taskCollection->countSuccessfulTasks() == $this->taskCollection->countExecutedTasks()) {
            $this->getStyle()->success($message);
        } else {
            $this->getStyle()->error($message);
        }
    }

    /**
     * @return void
     */
    protected function step()
    {
        $runningTasks = $this->taskCollection->countRunningTasks();
        $numberOfTasksToStart = ($this->maxTasks - $runningTasks);
        if ($numberOfTasksToStart > 0) {
            $tasksToStart = array_slice($this->getOpenTasks(), 0, $numberOfTasksToStart);
            foreach ($tasksToStart as $task) {
                /* @var $task AbstractTask */
                $task->start();
            }
            // update finished tasks
            $newFinishedTasks = $this->taskCollection->countExecutedTasks() - $this->finishedTasks;
            if ($newFinishedTasks > 0) {
                $this->getStyle()->progressAdvance($newFinishedTasks);
                $this->finishedTasks+= $newFinishedTasks;
            }
        }

    }

    /**
     * @return array
     */
    public function getTaskErrors()
    {
        return $this->taskCollection->getTaskErrors();
    }

    /**
     * @return array
     */
    protected function getOpenTasks()
    {
        $openTasks = array();
        foreach ($this->taskCollection as $task) {
            /* @var $task AbstractTask */
            if (!$task->isStarted()) {
                $openTasks[] = $task;
            }
        }
        return $openTasks;
    }
}
