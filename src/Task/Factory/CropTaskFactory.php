<?php
namespace Domi202\WraithPhp\Task\Factory;

use Domi202\WraithPhp\Domain\Model\Path;
use Domi202\WraithPhp\Task\Collection\TaskCollection;
use Domi202\WraithPhp\Task\Model\CropTask;

class CropTaskFactory extends AbstractTaskFactory
{
    /**
     * @param Path $path
     * @param string $directory
     * @return CropTask $task
     */
    protected function createTask(
        Path $path,
        $directory
    ) {
        $task = new CropTask();

        $task->setPath($path);
        $task->setDirectory($directory);
        $task->setScreenWidths($this->getScreenWidths());

        $task->initialize();
        return $task;
    }

    /**
     * @return TaskCollection
     */
    public function createTaskCollection()
    {
        $taskCollection = new TaskCollection();
        foreach ($this->getPaths() as $path) {
            $task = $this->createTask(
                $path,
                $this->getConfiguration()->getDirectory()
            );
            if ($task->isValid()) {
                $taskCollection->append($task);
            }
        }
        return $taskCollection;
    }
}
