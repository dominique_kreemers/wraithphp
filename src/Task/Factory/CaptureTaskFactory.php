<?php
namespace Domi202\WraithPhp\Task\Factory;

use Domi202\WraithPhp\Domain\Model\Path;
use Domi202\WraithPhp\Task\Collection\TaskCollection;
use Domi202\WraithPhp\Task\Model\CaptureTask;
use Domi202\WraithPhp\Utility\FileUtility;

class CaptureTaskFactory extends AbstractTaskFactory
{
    /**
     * @var string
     */
    protected $directory;

    /**
     * @var array
     */
    protected $domains = array();

    /**
     * @return string
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * @param string $directory
     * @return CaptureTaskFactory
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;
        return $this;
    }

    /**
     * @return array
     */
    public function getDomains()
    {
        return $this->domains;
    }

    /**
     * @param array $domains
     * @return AbstractTaskFactory
     */
    public function setDomains(array $domains)
    {
        $this->domains = $domains;
        return $this;
    }

    /**
     * @return void
     */
    public function prepareCaptureEnvironment()
    {
        $tmpDirectory = FileUtility::addTrailingSlash($this->getConfiguration()->getTempDirectory());
        $baseDirectory = FileUtility::addTrailingSlash(BASE_PATH) . 'src/JavaScript/';
        FileUtility::prepareTempDirectory($tmpDirectory);

        $javascriptFile = $this->getConfiguration()->getJavascriptFilename();
        FileUtility::copyFileIfNotExists(
            $tmpDirectory . $javascriptFile,
            $baseDirectory . $javascriptFile
        );
    }

    /**
     * @return void
     */
    public function cleanUpCaptureEnvironment()
    {
        FileUtility::cleanUpTempDirectory($this->getConfiguration()->getTempDirectory());
    }

    /**
     * @param Path $path
     * @param string $domain
     * @param array $screenWidths
     * @return CaptureTask $task
     */
    protected function createTask(
        Path $path,
        $domain,
        array $screenWidths
    ) {
        $task = new CaptureTask();

        $task->setDomain($domain);

        $task->setPath($path);
        $task->setScreenWidths($screenWidths);
        $task->setDirectory($this->getDirectory());

        $task->initialize();
        return $task;
    }

    /**
     * @return TaskCollection
     */
    public function createTaskCollection()
    {
        $taskCollection = new TaskCollection();
        foreach ($this->getPaths() as $path) {
            foreach ($this->getDomains() as $domain) {
                $taskCollection->append(
                    $this->createTask(
                        $path,
                        $domain,
                        $this->getScreenWidths()
                    )
                );
            }
        }
        return $taskCollection;
    }
}
