<?php
namespace Domi202\WraithPhp\Task\Factory;

use Domi202\WraithPhp\Domain\Model\Configuration;
use Domi202\WraithPhp\Task\Collection\TaskCollectionInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractTaskFactory
{
    use ContainerAwareTrait;

    /**
     * @param ContainerInterface $container
     * @return AbstractTaskFactory
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    /**
     * @return Configuration
     */
    protected function getConfiguration()
    {
        return $this->container->get('configuration');
    }

    /**
     * @return array
     */
    public function getPaths()
    {
        return $this->getConfiguration()->getPaths();
    }

    /**
     * @return array
     */
    public function getScreenWidths()
    {
        return $this->getConfiguration()->getScreenWidths();
    }

    /**
     * @return TaskCollectionInterface
     */
    abstract public function createTaskCollection();
}
