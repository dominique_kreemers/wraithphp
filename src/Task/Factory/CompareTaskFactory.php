<?php
namespace Domi202\WraithPhp\Task\Factory;

use Domi202\WraithPhp\Domain\Model\Path;
use Domi202\WraithPhp\Task\Collection\CompareTaskCollection;
use Domi202\WraithPhp\Task\Model\CompareTask;
use Domi202\WraithPhp\Utility\StringUtility;

class CompareTaskFactory extends AbstractTaskFactory
{
    /**
     * @param Path $path
     * @param string $screenWidth
     * @return CompareTask $task
     */
    protected function createTask(
        Path $path,
        $screenWidth
    ) {
        $task = new CompareTask();

        $task->setPath($path);
        $task->setScreenWidth($screenWidth);

        $task->initialize();
        return $task;
    }

    /**
     * @return CompareTaskCollection
     */
    public function createTaskCollection()
    {
        $compareTaskCollection = new CompareTaskCollection();
        foreach ($this->getPaths() as $path) {
            foreach ($this->getScreenWidths() as $screenWidth) {
                $task = $this->createTask(
                    $path,
                    $screenWidth
                );
                if ($task->canBeCompared()) {
                    $compareTaskCollection->append($task);
                }
            }
        }
        return $compareTaskCollection;
    }
}
