<?php
namespace Domi202\WraithPhp\Task\Collection;

use Domi202\WraithPhp\Task\Model\AbstractTask;
use Domi202\WraithPhp\Task\TaskInterface;

/**
 * Class TaskCollection
 * @package Domi202\WraithPhp\Task\Collection
 */
class TaskCollection extends \ArrayObject implements TaskCollectionInterface
{
    /**
     * @param TaskInterface $task
     * @return void
     */
    public function detach(TaskInterface $task)
    {
        foreach ($this as $index => $value) {
            if ($task === $value) {
                $this->offsetUnset($index);
                break;
            }
        }
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return ($this->count() > $this->countSuccessfulTasks());
    }

    /**
     * @return bool
     */
    public function hasRunningTasks()
    {
        foreach ($this as $task) {
            /* @var $task AbstractTask */
            if ($task->isRunning() || !$task->isStarted()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getTaskErrors()
    {
        $errors = array();
        foreach ($this as $task) {
            /* @var $task AbstractTask */
            if (!$task->getErrorOutput()) {
                continue;
            }
            $errors[] = $task->getErrorOutput();
        }
        return $errors;
    }

    /**
     * @return int
     */
    public function countRunningTasks()
    {
        $iterator = 0;
        foreach ($this as $task) {
            /* @var $task AbstractTask */
            if ($task->isRunning()) {
                $iterator++;
            }
        }
        return $iterator;
    }

    /**
     * @return int
     */
    public function countExecutedTasks()
    {
        $iterator = 0;
        foreach ($this as $task) {
            /* @var $task AbstractTask */
            if ($task->isSuccessful() || $task->isTerminated()) {
                $iterator++;
            }
        }
        return $iterator;
    }

    /**
     * @return int
     */
    public function countSuccessfulTasks()
    {
        $iterator = 0;
        foreach ($this as $task) {
            /* @var $task AbstractTask */
            if ($task->isSuccessful()) {
                $iterator++;
            }
        }
        return $iterator;
    }
}
