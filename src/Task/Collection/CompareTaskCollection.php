<?php
namespace Domi202\WraithPhp\Task\Collection;

use Domi202\WraithPhp\Task\Model\CompareTask;

/**
 * Class CompareTaskCollection
 * @package Domi202\WraithPhp\Task\Collection
 */
class CompareTaskCollection extends TaskCollection
{
    /**
     * @var string
     */
    const MODE_ALPHANUMERIC = 'alphanumeric';

    /**
     * @var string
     */
    const MODE_DIFFSFIRST = 'diffs_first';

    /**
     * @var string
     */
    const MODE_DIFFSONLY = 'diffs_only';

    /**
     * @return CompareTaskCollection
     */
    public function removeAllEqualTasks()
    {
        $remove = array();
        foreach ($this as $task) {
            /* @var $task CompareTask */
            if ($task->getDiff() === 0.0) {
                $remove[] = $task;
            }
        }
        if (count($remove)) {
            foreach ($remove as $task) {
                $this->detach($task);
            }
        }
    }

    /**
     * @return void
     */
    public function sortByDiff()
    {
        $this->uasort(function($a, $b) {
            /* @var $a CompareTask */
            /* @var $b CompareTask */
            if ($a->getDiff() == $b->getDiff()) {
                return 0;
            }
            return ($a->getDiff() < $b->getDiff()) ? 1 : -1;
        });
    }

    /**
     * @param string $mode
     * @return void
     */
    public function addFilterMode($mode = self::MODE_DIFFSFIRST)
    {
        switch ($mode) {
            case self::MODE_ALPHANUMERIC:
                // nothing to do
                break;
            case self::MODE_DIFFSFIRST:
                $this->sortByDiff();
                break;
            case self::MODE_DIFFSONLY:
                $this->removeAllEqualTasks();
                $this->sortByDiff();
                break;
        }
    }
}
