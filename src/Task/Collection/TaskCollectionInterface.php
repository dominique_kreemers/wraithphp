<?php
namespace Domi202\WraithPhp\Task\Collection;

use Domi202\WraithPhp\Task\TaskInterface;

/**
 * Interface TaskCollectionInterface
 * @package Domi202\WraithPhp\Task\Collection
 */
interface TaskCollectionInterface extends \IteratorAggregate, \ArrayAccess, \Serializable, \Countable
{
    /**
     * @param TaskInterface $task
     * @return void
     */
    public function detach(TaskInterface $task);

    /**
     * @return bool
     */
    public function hasRunningTasks();

    /**
     * @return array
     */
    public function getTaskErrors();

    /**
     * @return int
     */
    public function countRunningTasks();

    /**
     * @return int
     */
    public function countExecutedTasks();

    /**
     * @return int
     */
    public function countSuccessfulTasks();
}
