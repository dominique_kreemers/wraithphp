<?php
namespace Domi202\WraithPhp\Task\Model;

use Domi202\WraithPhp\Domain\Model\Path;
use Domi202\WraithPhp\Utility\FileUtility;
use Domi202\WraithPhp\Utility\StringUtility;

class CaptureTask extends AbstractTask
{
    /**
     * @var string
     */
    protected $domain;

    /**
     * @var Path
     */
    protected $path;

    /**
     * @var array
     */
    protected $screenWidths;

    /**
     * @var string
     */
    protected $directory = 'shots';

    /**
     * @return void
     */
    public function initialize()
    {
        $cmd = $this->buildCommand(
            $this->path->getConfiguration()->getBrowserWithOptions(),
            array(
                $this->getJavascriptFileName(),
                $this->getUrl(),
                $this->getScreenWidthString(),
                $this->getFilename(),
                $this->path->getSelector(),
                $this->getAbsolutePathForScript($this->path->getConfiguration()->getGlobalBeforeCapture()),
                $this->getAbsolutePathForScript($this->path->getBeforeCapture())
            )
        );
//        var_dump($cmd);

        parent::__construct($cmd);
    }

    /**
     * @return string
     */
    protected function getJavascriptFileName()
    {
        $path = FileUtility::addTrailingSlash(
            $this->getConfiguration()->getTempDirectory()
        );
        $path.= $this->getConfiguration()->getJavascriptFilename();
        return $path;
    }

    /**
     * @param null|string $scriptFile
     * @return null|string
     */
    public function getAbsolutePathForScript($scriptFile = null)
    {
        if ($scriptFile === null) {
            return null;
        }
        $absolutePath = $this->getWorkingDirectory() . '/' . $scriptFile;
        if (!file_exists($absolutePath)) {
            return null;
        }
        return $absolutePath;
    }

    /**
     * @return \Domi202\WraithPhp\Domain\Model\Configuration
     */
    public function getConfiguration()
    {
        return $this->path->getConfiguration();
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     * @return CaptureTask
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return Path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param Path $path
     * @return CaptureTask
     */
    public function setPath(Path $path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * @param string $directory
     * @return CaptureTask
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;
        return $this;
    }

    /**
     * @return array
     */
    public function getScreenWidths()
    {
        return $this->screenWidths;
    }

    /**
     * @return string
     */
    public function getScreenWidthString()
    {
        return implode(',', $this->screenWidths);
    }

    /**
     * @param array $screenWidths
     * @return CaptureTask
     */
    public function setScreenWidths(array $screenWidths)
    {
        $this->screenWidths = $screenWidths;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return implode('', array(
            rtrim($this->domain, '/'),
            $this->path->getPath(),
        ));
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        $directory = FileUtility::addTrailingSlash($this->directory) . $this->path->getName();
        FileUtility::createDirectory($directory);
        return implode('', array(
            $directory,
            '/',
            ($this->isMulti() ? 'MULTI' : $this->getScreenWidthString()),
            '.png',
        ));
    }

    /**
     * @return bool
     */
    protected function isMulti()
    {
        return (
            $this->getConfiguration()->getResizeOrReload() == 'resize'
            && count($this->getScreenWidths()) > 1
        );
    }

    /**
     * @param callable|null $callback
     * @return void
     */
    public function start(callable $callback = null)
    {
        $captureTask = $this;
        parent::start(function($status, $data) use ($captureTask) {
            if ($captureTask->getPath()->getConfiguration()->getVerbose()) {
                var_dump($status, $data);
            }
        });
    }
}
