<?php
namespace Domi202\WraithPhp\Task\Model;

use Domi202\WraithPhp\Domain\Model\Path;
use Domi202\WraithPhp\Utility\FileUtility;
use Domi202\WraithPhp\Utility\MathUtility;

class CompareTask extends AbstractTask
{
    /**
     * @var Path
     */
    protected $path;

    /**
     * @var string
     */
    protected $screenWidth;

    /**
     * @var number
     */
    protected $diff = false;

    /**
     * @var bool
     */
    protected $success = false;

    /**
     * @return void
     */
    public function initialize()
    {
        $cmd = $this->buildCommand(
            'compare',
            array(
                $this->getBaseImage(),
                $this->getCompareImage(),
                $this->getOverlayImage(),
            ),
            array(
                'dissimilarity-threshold' => '10',
                'fuzz' => $this->getConfiguration()->getFuzz(),
                'metric' => 'AE',
                'highlight-color' => $this->getConfiguration()->getHighlightColor(),
            )
        );
//        var_dump($cmd);

        parent::__construct($cmd);
    }

    /**
     * @return string
     */
    public function getBaseImage()
    {
        return $this->getFilename(
            $this->getConfiguration()->getDirectory(),
            '_base'
        );
    }

    /**
     * @return string
     */
    public function getCompareImage()
    {
        return $this->getFilename(
            $this->getConfiguration()->getDirectory()
        );
    }

    /**
     * @return string
     */
    public function getOverlayImage()
    {
        return $this->getCompareFilename(
            $this->getConfiguration()->getDirectory()
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->path->getName();
    }

    /**
     * @return bool
     */
    public function canBeCompared()
    {
        $result = FileUtility::filesExist(array(
            $this->getBaseImage(),
            $this->getCompareImage()
        ));
        return $result;
    }

    /**
     * @param string $directory
     * @param string $suffix
     * @return string
     */
    protected function getFilename($directory, $suffix = '')
    {
        return implode('/', array(
            rtrim($directory, '/'),
            rtrim($this->path->getName(), '/'),
            $this->getScreenWidth() . $suffix . '.png',
        ));
    }

    /**
     * @param string $directory
     * @return string
     */
    protected function getCompareFilename($directory)
    {
        return implode('/', array(
            rtrim($directory, '/'),
            rtrim($this->path->getName(), '/'),
            $this->getScreenWidth() . '_compare.png',
        ));
    }

    /**
     * @param string $directory
     * @return string
     */
    protected function getInfoFilename($directory)
    {
        return implode('/', array(
            rtrim($directory, '/'),
            rtrim($this->path->getName(), '/'),
            $this->getScreenWidth() . '_compare.txt',
        ));
    }

    /**
     * @return \Domi202\WraithPhp\Domain\Model\Configuration
     */
    public function getConfiguration()
    {
        return $this->path->getConfiguration();
    }

    /**
     * @return Path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param Path $path
     * @return CompareTask
     */
    public function setPath(Path $path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getScreenWidth()
    {
        $screenWidth = $this->checkScreenWidth($this->screenWidth);
        return $screenWidth;
    }

    /**
     * @param string $screenWidth
     * @return CompareTask
     */
    public function setScreenWidth($screenWidth)
    {
        $this->screenWidth = $screenWidth;
        return $this;
    }

    /**
     * @param string $screenWidth
     * @return string
     */
    protected function checkScreenWidth($screenWidth)
    {
        $parts = explode('x', $screenWidth);
        if (count($parts) == 1) {
            return $screenWidth;
        }
        array_pop($parts);
        return implode('', $parts);
    }

    /**
     * @return float
     */
    public function getDiff()
    {
        return $this->diff;
    }

    /**
     * @param float $diff
     * @return void
     */
    public function setDiff($diff)
    {
        $this->diff = (float) $diff;
    }

    /**
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param boolean $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        if ($this->getDiff() === false) {
            return false;
        }
        if ((float) $this->getConfiguration()->getThreshold() < (float) $this->getDiff()) {
            return false;
        }
        return true;
    }

    /**
     * @param int $diff
     * @return void
     */
    public function calculateDiff($diff)
    {
        $filePath = $this->getFilename(
            $this->getConfiguration()->getDirectory()
        );
        if (!file_exists($filePath)) {
            return null;
        }
        $imageInfo = getimagesize($filePath);
        $pixels = ($imageInfo[0] * $imageInfo[1]);
        $percent = round((($diff / $pixels) * 100), 2);
        $this->setDiff($percent);
    }

    /**
     * @return string|null
     */
    public function getErrorOutput()
    {
        $errorOutput = parent::getErrorOutput();
        if (
            !MathUtility::canBeInterpretedAsInteger($errorOutput)
            && !MathUtility::isExponentNumber($errorOutput)
        ) {
            return $errorOutput;
        }
    }

    /**
     * @param callable|null $callback
     * @return void
     */
    public function start(callable $callback = null)
    {
        $compareTask = $this;
        parent::start(function($status, $data) use ($compareTask) {
            if ($compareTask->getConfiguration()->getVerbose()) {
                var_dump($status, $data);
            }
            $filename = $compareTask->getInfoFilename(
                $compareTask->getConfiguration()->getDirectory()
            );
            $compareTask->calculateDiff($data);
            if ($compareTask->getDiff() !== false) {
                file_put_contents($filename, $compareTask->getDiff());
                $compareTask->setSuccess(true);
            }
        });
    }
}
