<?php
namespace Domi202\WraithPhp\Task\Model;

use Domi202\WraithPhp\Task\TaskInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessUtils;
use Symfony\Component\Process\Exception\RuntimeException;

/**
 * Class AbstractTask
 * @package Domi202\WraithPhp\Task\Model
 */
abstract class AbstractTask extends Process implements TaskInterface
{
    /**
     * Constructor.
     *
     * @param string         $commandline The command line to run
     * @param string|null    $cwd         The working directory or null to use the working dir of the current PHP process
     * @param array|null     $env         The environment variables or null to use the same environment as the current PHP process
     * @param mixed|null     $input       The input as stream resource, scalar or \Traversable, or null for no input
     * @param int|float|null $timeout     The timeout in seconds or null to disable
     * @param array          $options     An array of options for proc_open
     *
     * @throws RuntimeException When proc_open is not installed
     */
    public function __construct($commandline = null, $cwd = null, array $env = null, $input = null, $timeout = 60, array $options = array())
    {
        if ($commandline !== null) {
            parent::__construct($commandline, $cwd, $env, $input, $timeout, $options);
        }
    }

    /**
     * @param string $cmd
     * @param array $arguments
     * @param array $options
     * @return string
     */
    protected function buildCommand($cmd, array $arguments = array(), array $options = array())
    {
        return implode(' ', array_merge(
            array($cmd),
            $this->prepareOptions($options),
            $this->prepareArguments($arguments)
        ));
    }

    /**
     * @param array $options
     * @return array
     */
    protected function prepareOptions(array $options)
    {
        $result = array();
        foreach ($options as $optionName => $optionValue) {
            $result[] = '-' . $optionName . ' ' . $optionValue;
        }
        return $result;
    }

    /**
     * @param array $arguments
     * @return array
     */
    protected function prepareArguments(array $arguments)
    {
        foreach ($arguments as &$argument) {
            $argument = ProcessUtils::escapeArgument($argument);
        }
        return $arguments;
    }
}
