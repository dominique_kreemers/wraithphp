<?php
namespace Domi202\WraithPhp\Task\Model;
use Domi202\WraithPhp\Utility\FileUtility;

/**
 * Class CropTask
 * @package Domi202\WraithPhp\Task\Model
 */
class CropTask extends AbstractTask
{
    /**
     * @var Path
     */
    protected $path;

    /**
     * @var string
     */
    protected $directory;

    /**
     * @var array
     */
    protected $screenWidths = array();

    /**
     * @var array
     */
    protected $commands = array();

    /**
     * @var bool
     */
    protected $valid = false;

    /**
     * @return Path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param Path $path
     * @return CropTask
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * @param string $directory
     * @return CropTask
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;
        return $this;
    }

    /**
     * @return array
     */
    public function getScreenWidths()
    {
        return $this->screenWidths;
    }

    /**
     * @param array $screenWidths
     * @return CropTask
     */
    public function setScreenWidths($screenWidths)
    {
        $this->screenWidths = $screenWidths;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        return $this->valid;
    }

    /**
     * @return void
     */
    public function initialize()
    {
        foreach ($this->getScreenWidths() as $screenWidth) {
            $imagePaths = array();
            $directory = FileUtility::addTrailingSlash($this->getDirectory()) . $this->path->getName() . '/';
            $filePath =  $directory . $screenWidth . '.png';
            if (file_exists($filePath)) {
                $imagePaths[] = $filePath;
            }
            $baseFilePath = $directory . $screenWidth . '_base.png';
            if (file_exists($baseFilePath)) {
                $imagePaths[] = $baseFilePath;
            }
            if (count($imagePaths)) {
                list($width, $height) = $this->getMaxDimensions($imagePaths);
                $imagesToCrop = $this->getImagesToCrop($imagePaths, $width, $height);
                foreach ($imagesToCrop as $imageToCrop) {
                    $this->createCropCommand($imageToCrop, $width, $height);
                }
            }
        }

        if (count($this->commands)) {
            $cmd = implode(' & ', $this->commands);
//            var_dump($cmd);
            $this->valid = true;
            parent::__construct($cmd);
        }
    }

    /**
     * @param string $image
     * @param int $width
     * @param int $height
     */
    protected function createCropCommand($image, $width, $height)
    {
        $this->commands[] = $this->buildCommand(
            'convert',
            array(
                $image,
                $image
            ),
            array(
                'background' => 'none',
                'extent' => $width . 'x' . $height,
            )
        );
    }

    /**
     * @param array $images
     * @return array
     */
    protected function getMaxDimensions(array $images)
    {
        $maxWidth = null;
        $maxHeight = null;
        foreach ($images as $image) {
            list($width, $height) = getimagesize($image);
            if ($maxWidth === null || $maxWidth < $width) {
                $maxWidth = $width;
            }
            if ($maxHeight === null || $maxHeight < $height) {
                $maxHeight = $height;
            }
        }
        return array(
            $maxWidth,
            $maxHeight,
        );
    }

    /**
     * @param array $images
     * @param int $finalWidth
     * @param int $finalHeight
     * @return array
     */
    protected function getImagesToCrop(array $images, $finalWidth, $finalHeight)
    {
        $imagesToCrop = array();
        foreach ($images as $image) {
            list($width, $height) = getimagesize($image);
            if ($finalWidth != $width || $finalHeight != $height) {
                $imagesToCrop[] = $image;
            }
        }
        return $imagesToCrop;
    }
}
