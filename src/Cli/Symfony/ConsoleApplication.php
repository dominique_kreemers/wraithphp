<?php
namespace Domi202\WraithPhp\Cli\Symfony;

use Domi202\WraithPhp\Command\History\CropCommand;
use Domi202\WraithPhp\Command\ListCommand;
use Domi202\WraithPhp\Command\SpiderCommand;
use Domi202\WraithPhp\Command\ResetCommand;
use Domi202\WraithPhp\Command\History\HistoryCommand;
use Domi202\WraithPhp\Command\History\LatestCommand;
use Domi202\WraithPhp\Command\History\CompareCommand;
use Domi202\WraithPhp\Task\Factory\CaptureTaskFactory;
use Domi202\WraithPhp\Task\Factory\CompareTaskFactory;
use Domi202\WraithPhp\Task\Factory\CropTaskFactory;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class ConsoleApplication extends Application
{
    /**
     * @return ContainerBuilder
     */
    protected function buildContainer()
    {
        $container = new ContainerBuilder();

        $captureTaskFactoryDefinition = new Definition(CaptureTaskFactory::class);
        $captureTaskFactoryDefinition->setArguments(array($container));
        $container->setDefinition('capturetaskfactory', $captureTaskFactoryDefinition);

        $cropTaskFactoryDefinition = new Definition(CropTaskFactory::class);
        $cropTaskFactoryDefinition->setArguments(array($container));
        $container->setDefinition('croptaskfactory', $cropTaskFactoryDefinition);

        $compareTaskFactoryDefinition = new Definition(CompareTaskFactory::class);
        $compareTaskFactoryDefinition->setArguments(array($container));
        $container->setDefinition('comparetaskfactory', $compareTaskFactoryDefinition);

        return $container;
    }

    /**
     * Runs the current application.
     *
     * @param InputInterface  $input  An Input instance
     * @param OutputInterface $output An Output instance
     *
     * @return int 0 if everything went fine, or an error code
     *
     * @throws \Exception When doRun returns Exception
     */
    public function run(InputInterface $input = null, OutputInterface $output = null)
    {
        $container = $this->buildContainer();
        $commands = array(
            new ListCommand(),
            new SpiderCommand(),
            new HistoryCommand(),
            new LatestCommand(),
            new CompareCommand(),
            new CropCommand(),
            new ResetCommand(),
        );
        foreach ($commands as $currentCommand) {
            /* @var $command \Domi202\WraithPhp\Command\AbstractCommand */
            $currentCommand->setContainer($container);
        }

        $this->addCommands($commands);
        $this->setDefaultCommand(reset($commands)->getName());

        return parent::run($input, $output);
    }
}
