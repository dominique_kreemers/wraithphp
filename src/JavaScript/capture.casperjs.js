/* jshint node: true */
"use strict";
var system = require('system'),
    casper = require('casper').create({
        httpStatusHandlers: {
            404: function(casper, resource) {
                system.stderr.write('404 Not Found: ' + resource.url);
                casper.exit(1);
            },
            500: function(casper, resource) {
                system.stderr.write('500 Internal Server Error: ' + resource.url);
                casper.exit(1);
            }
        },
        onError: function(errMsg) {
            system.stderr.write(errMsg);
            casper.exit(1);
        }
    });

if (casper.cli.get('verbose') === true) {
    casper.options.verbose = true;
    casper.options.logLevel = "debug";
}

/**
 * @param {String} commandLineDimensions
 * @constructor
 */
var CaptureHelper = function(commandLineDimensions) {
    var multipleDimensions = commandLineDimensions.toString().split(',');

    if (multipleDimensions.length > 1) {
        this.dimensionsToPass = multipleDimensions.map((function (cliDimensions) {
            return this.getWidthAndHeight(cliDimensions);
        }).bind(this));
    } else {
        this.dimensionsToPass = this.getWidthAndHeight(commandLineDimensions);
    }
};
CaptureHelper.prototype = {
    /**
     * @param {String} dimensions
     * @returns {{viewportWidth: Number, viewportHeight: Number}}
     */
    getWidthAndHeight: function(dimensions) {
        dimensions = /(\d*)x?((\d*))?/i.exec(dimensions);
        return {
            'viewportWidth':  parseInt(dimensions[1]),
            'viewportHeight': parseInt(dimensions[2] || 1500)
        };
    },

    /**
     * @returns {Boolean}
     */
    takeMultipleScreenshots: function () {
        return (this.dimensionsToPass.length && this.dimensionsToPass.length > 1);
    },

    /**
     * @return {Array}
     */
    getDimensions: function() {
        return this.dimensionsToPass;
    },

    /**
     * Convert
     * shots/clickable_guide__after_click/MULTI_casperjs_english.png
     * to
     * shots/clickable_guide__after_click/1024x359_casperjs_english.png
     *
     * @return {String}
     */
    replaceImageNameWithDimensions: function(image_name, currentDimensions) {
        var dirs = image_name.split('/'),
            filename = dirs[dirs.length - 1],
            filenameParts = filename.split('_'),
            filenameExtension = filename.split('.')[filename.split('.').length - 1],
            newFilename;

        // filenameParts[0] = currentDimensions.viewportWidth + 'x' + currentDimensions.viewportHeight;
        filenameParts[0] = currentDimensions.viewportWidth;
        dirs.pop(); // remove MULTI_casperjs_english.png
        newFilename = dirs.join('/') + '/' + filenameParts.join('_') + '.' + filenameExtension;
        return newFilename;
    }

};

var helper = new CaptureHelper(casper.cli.get(1));

// command line arguments
var url = casper.cli.get(0),
    // dimensions = helper.dimensions,
    dimensions = helper.getDimensions(),
    image_name = casper.cli.get(2),
    selector = casper.cli.get(3),
    globalBeforeCaptureJS = casper.cli.get(4),
    pathBeforeCaptureJS = casper.cli.get(5),
    dimensionsProcessed = 0,
    currentDimensions;

/**
 *
 */
function snap() {
    // console.log('Snapping ' + url + ' at: ' + currentDimensions.viewportWidth + 'x' + currentDimensions.viewportHeight);
    try {
        if (!selector) {
            this.capture(image_name);
        } else {
            this.captureSelector(image_name, selector);
        }
    } catch(error) {
        system.stderr.write('Selector "' + selector + '" not found on: ' + url);
        casper.exit(error.message);
    }

    dimensionsProcessed++;
    if (helper.takeMultipleScreenshots() && dimensionsProcessed < dimensions.length) {
        currentDimensions = dimensions[dimensionsProcessed];
        image_name = helper.replaceImageNameWithDimensions(image_name, currentDimensions);
        casper.viewport(currentDimensions.viewportWidth, currentDimensions.viewportHeight);
        casper.wait(300, function then () {
            snap.bind(this)();
        });
    }
}

if (helper.takeMultipleScreenshots()) {
    currentDimensions = dimensions[0];
    image_name = helper.replaceImageNameWithDimensions(image_name, currentDimensions);
} else {
    currentDimensions = dimensions;
}

// Casper can now do its magic
casper.start();
casper.open(url);
casper.viewport(currentDimensions.viewportWidth, currentDimensions.viewportHeight);
casper.then(function() {
    var self = this;
    if (globalBeforeCaptureJS && pathBeforeCaptureJS) {
        require(globalBeforeCaptureJS)(self, function thenExecuteOtherBeforeCaptureFile() {
            require(pathBeforeCaptureJS)(self, captureImage);
        });
    } else if (globalBeforeCaptureJS) {
        require(globalBeforeCaptureJS)(self, captureImage);
    } else if (pathBeforeCaptureJS) {
        require(pathBeforeCaptureJS)(self, captureImage);
    } else {
        captureImage();
    }
});

/**
 *
 */
function captureImage() {
    // waits for all images to download before taking screenshots
    // (broken images are a big cause of Wraith failures!)
    // Credit: http://reff.it/8m3HYP
    casper.waitFor(function() {
        return this.evaluate(function() {
            var images = document.getElementsByTagName('img');
            return Array.prototype.every.call(images, function(i) { return i.complete; });
        });
    }, function then () {
        snap.bind(this)();
    });
}

casper.run();
