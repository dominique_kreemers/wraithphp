/* jshint node: true */
/* jshint phantom: true */
'use strict';
// modules
var system = require('system');
var page = require('webpage').create();

/**
 * @param {String} commandLineDimensions
 * @constructor
 */
var CaptureHelper = function(commandLineDimensions) {
    var multipleDimensions = commandLineDimensions.toString().split(',');
    if (multipleDimensions.length > 1) {
        this.dimensionsToPass = multipleDimensions.map((function (cliDimensions) {
            return this.getWidthAndHeight(cliDimensions);
        }).bind(this));
    } else {
        this.dimensionsToPass = this.getWidthAndHeight(commandLineDimensions);
    }
};
CaptureHelper.prototype = {
    /**
     * @param {String} dimensions
     * @returns {{viewportWidth: Number, viewportHeight: Number}}
     */
    getWidthAndHeight: function(dimensions) {
        dimensions = /(\d*)x?((\d*))?/i.exec(dimensions);
        return {
            'viewportWidth':  parseInt(dimensions[1]),
            'viewportHeight': parseInt(dimensions[2] || 1500)
        };
    },

    /**
     * @returns {Boolean}
     */
    takeMultipleScreenshots: function () {
        return (this.dimensionsToPass.length && this.dimensionsToPass.length > 1);
    },

    /**
     * @return {Array}
     */
    getDimensions: function() {
        return this.dimensionsToPass;
    },

    /**
     * Convert
     * shots/clickable_guide__after_click/MULTI_casperjs_english.png
     * to
     * shots/clickable_guide__after_click/1024x359_casperjs_english.png
     *
     * @return {String}
     */
    replaceImageNameWithDimensions: function(image_name, currentDimensions) {
        var dirs = image_name.split('/'),
            filename = dirs[dirs.length - 1],
            filenameParts = filename.split('_'),
            filenameExtension = filename.split('.')[filename.split('.').length - 1],
            newFilename;

        // filenameParts[0] = currentDimensions.viewportWidth + 'x' + currentDimensions.viewportHeight;
        filenameParts[0] = currentDimensions.viewportWidth;
        dirs.pop(); // remove MULTI_casperjs_english.png
        newFilename = dirs.join('/') + '/' + filenameParts.join('_') + '.' + filenameExtension;
        return newFilename;
    }
};

var helper = new CaptureHelper(system.args[2]);

// command line arguments
var url = system.args[1];
var dimensions = helper.getDimensions();
var imageName = system.args[3];
// var selector = system.args[4];
var globalBeforeCaptureJS = system.args[5];
var pathBeforeCaptureJS = system.args[6];
var dimensionsProcessed = 0;
var currentDimensions;
var currentRequests = 0;

globalBeforeCaptureJS = globalBeforeCaptureJS === 'false' ? false : globalBeforeCaptureJS;
pathBeforeCaptureJS = pathBeforeCaptureJS === 'false' ? false : pathBeforeCaptureJS;

var setupJavaScriptRan = false;
var waitTime = 300;
var maxWait = 5000;
var beenLoadingFor = 0;

if (helper.takeMultipleScreenshots(dimensions)) {
    currentDimensions = dimensions[0];
    imageName = helper.replaceImageNameWithDimensions(imageName, currentDimensions);
} else {
    currentDimensions = dimensions;
}

page.settings = {
    loadImages: true,
    javascriptEnabled: true
};
page.settings.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.17';

page.onError = function(msg, trace) {
    // suppress JS errors from Wraith output
    // http://stackoverflow.com/a/19538646
};

page.onResourceRequested = function(req) {
    currentRequests += 1;
};

page.onResourceReceived = function(res) {
    if (res.stage === 'end') {
        currentRequests -= 1;
    }
};

console.log('Loading ' + url + ' at dimensions: ' + currentDimensions.viewportWidth + 'x' + currentDimensions.viewportHeight);
page.viewportSize = { width: currentDimensions.viewportWidth, height: currentDimensions.viewportHeight};

page.open(url, function(status) {
    if (status !== 'success') {
        console.log('Error with page ' + url);
        phantom.exit();
    }

    setTimeout(checkStatusOfAssets, waitTime);
});


function checkStatusOfAssets() {
    if (currentRequests >= 1) {
        if (beenLoadingFor > maxWait) {
            // sometimes not all assets will download in an acceptable time - continue anyway.
            markPageAsLoaded();
        } else {
            beenLoadingFor += waitTime;
            setTimeout(checkStatusOfAssets, waitTime);
        }
    } else {
        markPageAsLoaded();
    }
}

function markPageAsLoaded() {
    if (!setupJavaScriptRan) {
        runSetupJavaScriptThen(captureImage);
    } else {
        captureImage();
    }
}


function runSetupJavaScriptThen(callback) {
    setupJavaScriptRan = true;
    if (globalBeforeCaptureJS && pathBeforeCaptureJS) {
        require(globalBeforeCaptureJS)(page, function thenExecuteOtherBeforeCaptureFile() {
            require(pathBeforeCaptureJS)(page, callback);
        });
    } else if (globalBeforeCaptureJS) {
        require(globalBeforeCaptureJS)(page, callback);
    } else if (pathBeforeCaptureJS) {
        require(pathBeforeCaptureJS)(page, callback);
    } else {
        callback();
    }
}

function captureImage() {
    takeScreenshot();
    dimensionsProcessed++;
    if (helper.takeMultipleScreenshots(dimensions) && dimensionsProcessed < dimensions.length) {
        currentDimensions = dimensions[dimensionsProcessed];
        imageName = helper.replaceImageNameWithDimensions(imageName, currentDimensions);
        setTimeout(resizeAndCaptureImage, waitTime);
    } else {
        exitPhantom();
    }
}

function resizeAndCaptureImage() {
    console.log('Resizing ' + url + ' to: ' + currentDimensions.viewportWidth + 'x' + currentDimensions.viewportHeight);
    page.viewportSize = { width: currentDimensions.viewportWidth, height: currentDimensions.viewportHeight};
    setTimeout(captureImage, 5000); // give page time to re-render properly
}

function takeScreenshot() {
    console.log('Snapping ' + url + ' at: ' + currentDimensions.viewportWidth + 'x' + currentDimensions.viewportHeight);
    page.clipRect = {
        top: 0,
        left: 0,
        height: currentDimensions.viewportHeight,
        width: currentDimensions.viewportWidth
    };
    page.render(imageName);
}

function exitPhantom() {
    // prevent CI from failing from
    // 'Unsafe JavaScript attempt to access frame with URL about:blank from frame with URL'
    // errors. See https://github.com/n1k0/casperjs/issues/1068
    setTimeout(function() {
        phantom.exit();
    }, 30);
}
