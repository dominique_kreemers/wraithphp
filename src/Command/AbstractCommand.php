<?php
namespace Domi202\WraithPhp\Command;

use Domi202\WraithPhp\Domain\Model\Configuration;
use Domi202\WraithPhp\Gallery\Renderer;
use Domi202\WraithPhp\Task\Collection\CompareTaskCollection;
use Domi202\WraithPhp\Task\Collection\TaskCollectionInterface;
use Domi202\WraithPhp\Task\TaskManager;
use Domi202\WraithPhp\Utility\FileUtility;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\Definition;

abstract class AbstractCommand extends Command
{
    use ContainerAwareTrait;

    /**
     * @return Configuration
     */
    protected function getConfiguration()
    {
        return $this->container->get('configuration');
    }

    /**
     * @return SymfonyStyle
     */
    protected function getStyle()
    {
        return $this->container->get('style');
    }

    /**
     * @param int $mode
     * @return void
     */
    protected function registerConfigurationArgument($mode = InputArgument::REQUIRED)
    {
        $this->addArgument('config', $mode);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function initialize(InputInterface $input, OutputInterface $output)
    {
        $styleDefinition = new Definition(SymfonyStyle::class, array($input, $output));
        $this->container->setDefinition('style', $styleDefinition);

        if ($input->hasArgument('config') && $input->getArgument('config')) {
            $configurationDefinition = new Definition(Configuration::class, array($input->getArgument('config')));
            $configurationDefinition->addMethodCall(
                'setContainer',
                array($this->container)
            );
            $this->container->setDefinition('configuration', $configurationDefinition);
        }
    }

    /**
     * @param TaskCollectionInterface $taskCollection
     * @return TaskManager
     */
    protected function createTaskManager(TaskCollectionInterface $taskCollection)
    {
        return new TaskManager(
            $taskCollection,
            $this->container
        );
    }

    /**
     * @param string $destinationDirectory
     * @param CompareTaskCollection $compareTaskCollection
     * @return string
     */
    protected function generateGallery($destinationDirectory, CompareTaskCollection $compareTaskCollection)
    {
        $renderer = new Renderer();
        $html = $renderer->render('Default.php', array(
            'tasks' => $compareTaskCollection
        ));
        $galleryFile = FileUtility::addTrailingSlash($destinationDirectory) . 'Gallery.html';
        file_put_contents($galleryFile, $html);
        return $galleryFile;
    }
}
