<?php
namespace Domi202\WraithPhp\Command;

use Domi202\WraithPhp\Utility\FileUtility;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

use VDB\Spider\Discoverer\CssSelectorDiscoverer;
use Symfony\Component\EventDispatcher\Event;
use VDB\Spider\Event\SpiderEvents;
use VDB\Spider\QueueManager\InMemoryQueueManager;
use VDB\Spider\Spider;
use VDB\Spider\Filter\Prefetch\UriFilter;

use VDB\Spider\Filter\Prefetch\AllowedHostsFilter;
use VDB\Spider\Filter\Prefetch\UriWithHashFragmentFilter;
use VDB\Spider\Filter\Prefetch\UriWithQueryStringFilter;

use Domi202\WraithPhp\Spider\Filter\Postfetch\MimeTypeFilter;

/**
 * Class SpiderCommand
 * @package Domi202\WraithPhp\Command
 */
class SpiderCommand extends AbstractCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('spider')
            ->setDescription('spider')
            ->setHelp('spider');

        $this->addOption(
                'domain',
                null,
                InputOption::VALUE_REQUIRED,
                'Domain to crawl'
            )->addOption(
                'file',
                'f',
                InputOption::VALUE_REQUIRED,
                'Write urls to file',
                'spider.txt'
            );
        $this->registerConfigurationArgument(InputArgument::OPTIONAL);
    }

    /**
     * @param string $url
     * @return string
     */
    protected function getQueryString($url)
    {
        return parse_url($url, PHP_URL_PATH);
    }

    /**
     * @param string $domain
     * @param int $maxDepth
     * @return Spider
     */
    protected function getSpider($domain, $maxDepth = 3)
    {
        $spider = new Spider($domain);

        $spider->getDiscovererSet()->set(new CssSelectorDiscoverer("a"));
        $spider->getDiscovererSet()->maxDepth = $maxDepth;

        // prefetch filters
        $preFilters = array(
            new AllowedHostsFilter(array($domain), false),
            new UriWithHashFragmentFilter(),
            new UriWithQueryStringFilter(),
            new UriFilter(array()),
        );
        foreach ($preFilters as $filter) {
            $spider->getDiscovererSet()->addFilter($filter);
        }

        // postfetch filters
        $postFilters = array(
            new MimeTypeFilter('text/html')
        );
        foreach ($postFilters as $filter) {
            $spider->getDownloader()->addPostFetchFilter($filter);
        }

        $style = $this->getStyle();
        $spider->getDispatcher()->addListener(
            SpiderEvents::SPIDER_CRAWL_USER_STOPPED,
            function (Event $event) use ($style) {
                $style->error('Crawl aborted by user');
                exit();
            }
        );
        $spider->getDispatcher()->addListener(
            SpiderEvents::SPIDER_CRAWL_RESOURCE_PERSISTED,
            function (Event $event) use ($style) {
                /* @var $event \Symfony\Component\EventDispatcher\GenericEvent */
                $style->text(implode(' ', array(
                    'Crawling',
                    $event->getArgument('uri')->__toString(),
                    '(Memory usage: ' . FileUtility::formatFilesize(memory_get_usage(true)) . ')',
                )));
            }
        );

        $queueManager = new InMemoryQueueManager();
        $queueManager->setTraversalAlgorithm(InMemoryQueueManager::ALGORITHM_BREADTH_FIRST);
        $spider->setQueueManager($queueManager);

        return $spider;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getStyle()->title('WraithPhp Spider');

        $domain = $input->getOption('domain');
        $domain = FileUtility::addTrailingSlash($domain);
        $spider = $this->getSpider($domain, 1);
        $spider->crawl();

        $urls = array();
        foreach ($spider->getDownloader()->getPersistenceHandler() as $resource) {
            $uri = $this->getQueryString($resource->getUri());
            if ($uri && strlen($uri)) {
                $urls[] = $uri;
            }
        }

        if ($input->getOption('file')) {
            $fileName = $input->getOption('file');
            file_put_contents($fileName, implode("\n", $urls));
        }

    }
}
