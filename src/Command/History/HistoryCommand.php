<?php
namespace Domi202\WraithPhp\Command\History;

use Domi202\WraithPhp\Exception;
use Domi202\WraithPhp\Utility\FileUtility;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class HistoryCommand
 * @package Domi202\WraithPhp\Command\History
 */
class HistoryCommand extends AbstractHistoryCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        // TODO: define
        $this
            ->setName('history')
            ->setDescription('')
            ->setHelp('');

        $this->registerConfigurationArgument();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            FileUtility::emptyDirectory($this->getDestinationDirectory());
            $this->capture();
        } catch (Exception $e) {
            return 1;
        }
    }

    /**
     * @return string
     */
    protected function getDestinationDirectory()
    {
        return $this->getConfiguration()->getHistoryDir();
    }
}
