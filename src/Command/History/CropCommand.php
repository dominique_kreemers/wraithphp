<?php
namespace Domi202\WraithPhp\Command\History;

use Domi202\WraithPhp\Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CropCommand
 * @package Domi202\WraithPhp\Command\History
 */
class CropCommand extends AbstractHistoryCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        // TODO: define
        $this
            ->setName('crop')
            ->setDescription('')
            ->setHelp('');

        $this->registerConfigurationArgument();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->crop();
        } catch (Exception $e) {
            return 1;
        }
    }

    /**
     * @return string
     */
    protected function getDestinationDirectory()
    {
        return $this->getConfiguration()->getDirectory();
    }
}
