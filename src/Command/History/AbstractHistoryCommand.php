<?php
namespace Domi202\WraithPhp\Command\History;

use Domi202\WraithPhp\Command\AbstractCommand;
use Domi202\WraithPhp\Exception;
use Domi202\WraithPhp\Task\Collection\CompareTaskCollection;
use Domi202\WraithPhp\Task\Collection\TaskCollection;
use Domi202\WraithPhp\Task\Collection\TaskCollectionInterface;
use Domi202\WraithPhp\Task\Factory\CaptureTaskFactory;
use Domi202\WraithPhp\Task\Factory\CompareTaskFactory;
use Domi202\WraithPhp\Task\Factory\CropTaskFactory;
use Domi202\WraithPhp\Task\TaskManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Domi202\WraithPhp\Gallery\Renderer;

/**
 * Class AbstractHistoryCommand
 * @package Domi202\WraithPhp\Command\History
 */
abstract class AbstractHistoryCommand extends AbstractCommand
{
    /**
     * @return void
     */
    protected function capture()
    {
        /* @var $captureTaskFactory CaptureTaskFactory */
        $captureTaskFactory = $this->container->get('capturetaskfactory');
        $captureTaskFactory
            ->setDirectory($this->getDestinationDirectory())
            ->setDomains($this->getConfiguration()->getDomains());

        $taskCollection = $captureTaskFactory->createTaskCollection();
        $taskManager = $this->createTaskManager($taskCollection);

        $captureTaskFactory->prepareCaptureEnvironment();

        $this->container->get('style')->title('Capturing ' . $taskCollection->count() . ' pages');
        $taskManager->runTasksAsync();

        $captureTaskFactory->cleanUpCaptureEnvironment();

        $failedTaskMessages = $taskManager->getTaskErrors();
        if (count($failedTaskMessages)) {
            foreach ($failedTaskMessages as $failedTaskMessage) {
                $this->getStyle()->error($failedTaskMessage);
            }
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    protected function compare()
    {
        $this->crop();

        /* @var $compareTaskFactory CompareTaskFactory */
        $compareTaskFactory = $this->container->get('comparetaskfactory');
        $compareTaskCollection = $compareTaskFactory->createTaskCollection();

        $taskManager = $this->createTaskManager($compareTaskCollection);

        $this->getStyle()->title('Comparing ' . $compareTaskCollection->count() . ' paths');
        $taskManager->runTasksAsync();

        $failedTaskMessages = $taskManager->getTaskErrors();
        if (count($failedTaskMessages)) {
            foreach ($failedTaskMessages as $failedTaskMessage) {
                $this->getStyle()->error($failedTaskMessage);
            }
        }

        $compareTaskCollection->addFilterMode(
            $this->getConfiguration()->getMode()
        );

        $galleryFile = $this->generateGallery(
            $this->getDestinationDirectory(),
            $compareTaskCollection
        );

        if ($compareTaskCollection->hasErrors()) {
            throw new Exception('Compare has exited with errors. Take a look in the gallery at ' . $galleryFile);
        }
    }

    /**
     * @return void
     */
    protected function crop()
    {
        /* @var $cropTaskFactory CropTaskFactory */
        $cropTaskFactory = $this->container->get('croptaskfactory');
        $taskCollection = $cropTaskFactory->createTaskCollection();

        $taskManager = $this->createTaskManager($taskCollection);
        if ($taskCollection->count() > 0) {
            $this->getStyle()->title('Cropping ' . $taskCollection->count() . ' images');
            $taskManager->runTasksAsync();

            $failedTaskMessages = $taskManager->getTaskErrors();
            if (count($failedTaskMessages)) {
                foreach ($failedTaskMessages as $failedTaskMessage) {
                    $this->getStyle()->error($failedTaskMessage);
                }
            }
        }
    }

    /**
     * @return string
     */
    abstract protected function getDestinationDirectory();
}
