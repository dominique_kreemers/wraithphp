<?php
namespace Domi202\WraithPhp\Command\History;

use Domi202\WraithPhp\Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CompareCommand
 * @package Domi202\WraithPhp\Command\History
 */
class CompareCommand extends AbstractHistoryCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        // TODO: define
        $this
            ->setName('compare')
            ->setDescription('')
            ->setHelp('');

        $this->registerConfigurationArgument();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->compare();
        } catch (Exception $e) {
            $this->getStyle()->error($e->getMessage());
            return 1;
        }
    }

    /**
     * @return string
     */
    protected function getDestinationDirectory()
    {
        return $this->getConfiguration()->getDirectory();
    }
}
