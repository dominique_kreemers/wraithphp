<?php
namespace Domi202\WraithPhp\Command;

use Domi202\WraithPhp\Utility\FileUtility;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetCommand extends AbstractCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        // TODO: define
        $this
            ->setName('reset')
            ->setDescription('')
            ->setHelp('');

        $this->registerConfigurationArgument();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        FileUtility::emptyDirectory($this->getConfiguration()->getDirectory());
        FileUtility::emptyDirectory($this->getConfiguration()->getHistoryDir());
        $this->getStyle()->success('All folders empty');
    }
}
