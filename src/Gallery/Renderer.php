<?php
namespace Domi202\WraithPhp\Gallery;

use Domi202\WraithPhp\Gallery\Helper\ImageHelper;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;

class Renderer
{
    /**
     * @var FilesystemLoader
     */
    protected $loader;

    /**
     * @return Renderer
     */
    public function __construct()
    {
        $this->loader = new FilesystemLoader(__DIR__ . '/Views/%name%');
        $this->templating = new PhpEngine(
            new TemplateNameParser(),
            $this->loader,
            array(
                new ImageHelper()
            )
        );
    }

    /**
     * @param string $template
     * @param array $data
     * @return string
     */
    public function render($template, array $data = array())
    {
        return $this->templating->render($template, $data);
    }
}
