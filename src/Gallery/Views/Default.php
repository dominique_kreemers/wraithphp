<!DOCTYPE html>
<html>
    <head>
        <title>WraithPhp Gallery</title>
        <style>
            body, table {
                font-family: "Arial";
            }
            div.main-item {
                border-bottom: 5px dotted #efefef;
                padding-bottom: 20px;
            }
            div.container {
                display: flex;
                justify-content: center;
            }
            div.container.headline {
                cursor: pointer;
            }
            div.image {

            }
            div.image img {
                width: 100%;
                height: auto;
            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script>
            (function($) {
                "use strict";
                $.fn.accordeon = function() {
                    return $(this).each(function() {
                        var $element = $(this);
                        var $headline = $element.find('div.headline');
                        var $imagesContainer = $element.find('div.images');
                        $imagesContainer.hide();
                        $headline.click(function(e) {
                            e.preventDefault();
                            if ($imagesContainer.is('.open')) {
                                $imagesContainer.removeClass('open').slideUp();
                            } else {
                                $imagesContainer.addClass('open').slideDown();
                            }

                        });
                    });
                };
                $(document).ready(function() {
                    $('div.main-item').accordeon();
                });
            })(jQuery);
        </script>
    </head>
    <body>
        <?php foreach($tasks as $task): ?>
            <?php /* @var $task \Domi202\WraithPhp\Task\Model\CompareTask */ ?>
            <?php if (!$task->isSuccess()) { continue; } ?>
            <div class="main-item">
                <div class="container headline">
                    <h1>
                        <?php echo $task->getName(); ?> /
                        <?php echo $task->getScreenWidth(); ?> /
                        <?php echo number_format($task->getDiff(), 2); ?>%
                    </h1>
                </div>
                <div class="container images">
                    <div class="image">
                        <img src="<?php echo $view['image']->path($task->getBaseImage()); ?>" alt="" />
                    </div>
                    <div class="image">
                        <img src="<?php echo $view['image']->path($task->getCompareImage()); ?>" alt="" />
                    </div>
                    <div class="image">
                        <img src="<?php echo $view['image']->path($task->getOverlayImage()); ?>" alt="" />
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </body>
</html>
