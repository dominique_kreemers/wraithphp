<?php
namespace Domi202\WraithPhp\Gallery\Helper;

use Symfony\Component\Templating\Helper\Helper;

/**
 * Class ImageHelper
 */
class ImageHelper extends Helper
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'image';
    }

    /**
     * @param string $path
     * @return string
     */
    public function path($path)
    {
        $parts = explode('/', $path);
        array_shift($parts);
        return implode('/', $parts);
    }
}