<?php
namespace Domi202\WraithPhp\Domain\Model;

use Domi202\WraithPhp\Utility\FileUtility;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Yaml\Yaml;
use Domi202\WraithPhp\Exception;

class Configuration
{
    use ContainerAwareTrait;

    /**
     * @var array
     */
    protected $defaults = array(
        'browser' => 'casperjs',
        'phantomjs_options' => '',
        'highlight_color' => 'blue',
        'resize_or_reload' => 'reload',
        'before_capture' => null,
        'history_dir' => 'shots_base',
        'directory' => 'shots',
        'mode' => 'diffs_first',
        'max_tasks' => 3,
        'verbose' => false,
        'temp_directory' => './.tmp',
    );

    /**
     * @var array
     */
    protected $configuration = array();

    /**
     * @var Path[]
     */
    protected $paths;

    /**
     * @param string $file
     * @return Configuration
     * @throws Exception
     */
    public function __construct($file)
    {
        if (!file_exists($file)) {
            throw new Exception('Yaml file ' . $file . ' not found');
        }
        $this->configuration = Yaml::parse(file_get_contents($file));
        $this->configuration = $this->loadImports(
            $this->configuration,
            dirname($file)
        );
    }

    /**
     * @param array $configuration
     * @param string $path
     * @return array
     * @throws Exception
     */
    protected function loadImports(array $configuration, $path)
    {
        if (isset($configuration['imports']) && is_array($configuration['imports'])) {
            $includeConfig = array();
            foreach ($configuration['imports'] as $importFile) {
                $filePath = FileUtility::addTrailingSlash($path) . $importFile['resource'];
                if (!file_exists($filePath)) {
                    throw new Exception('File ' . $filePath . ' not found');
                    continue;
                }
                $importArray = Yaml::parse(file_get_contents($filePath));
                $importArray = $this->loadImports(
                    $importArray,
                    dirname($filePath)
                );
                $includeConfig = array_merge_recursive(
                    $includeConfig,
                    $importArray
                );
            }
            $configuration = array_merge_recursive(
                $includeConfig,
                $configuration
            );
            unset($configuration['imports']);
        }
        return $configuration;
    }

    /**
     * @return array
     */
    public function getConfiguration()
    {
       return $this->configuration;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getConfigurationByKey($key)
    {
        if (isset($this->configuration[$key])) {
            return $this->configuration[$key];
        }
        if ($this->defaults[$key]) {
            return $this->defaults[$key];
        }
        return null;
    }

    /**
     * @return string
     */
    public function getBrowser()
    {
        return $this->getConfigurationByKey('browser');
    }

    /**
     * @return string
     */
    public function getBrowserOptions()
    {
        $options = $this->getConfigurationByKey('phantomjs_options');
        if (
            $this->getBrowser() == 'casperjs'
            && $this->getVerbose()
        ) {
            $options.= ' --verbose=true';
        }
        return $options;
    }

    /**
     * @return string
     */
    public function getBrowserWithOptions()
    {
        return implode(' ', array(
            $this->getBrowser(),
            $this->getBrowserOptions(),
        ));
    }

    /**
     * @return array
     */
    public function getScreenWidths()
    {
        return $this->configuration['screen_widths'];
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->getConfigurationByKey('mode');
    }

    /**
     * @return int
     */
    public function getFuzz()
    {
        return (int) $this->getConfigurationByKey('fuzz');
    }

    /**
     * @return int
     */
    public function getThreshold()
    {
        return (int) $this->getConfigurationByKey('threshold');
    }

    /**
     * @return string
     */
    public function getHistoryDir()
    {
        return $this->getConfigurationByKey('history_dir');
    }

    /**
     * @return string
     */
    public function getDirectory()
    {
        return $this->getConfigurationByKey('directory');
    }

    /**
     * @return string
     */
    public function getResizeOrReload()
    {
        return $this->getConfigurationByKey('resize_or_reload');
    }

    /**
     * @return null|string
     */
    public function getGlobalBeforeCapture()
    {
        return $this->getConfigurationByKey('before_capture');
    }

    /**
     * @return array
     */
    public function getDomains()
    {
        return $this->getConfigurationByKey('domains');
    }

    /**
     * @return string
     */
    public function getHighlightColor()
    {
        return $this->getConfigurationByKey('highlight_color');
    }

    /**
     * @return int
     */
    public function getMaxTasks()
    {
        return (int) $this->getConfigurationByKey('max_tasks');
    }

    /**
     * @return bool
     */
    public function getVerbose()
    {
        return (bool) $this->getConfigurationByKey('verbose');
    }

    /**
     * @return string
     */
    public function getTempDirectory()
    {
        return $this->getConfigurationByKey('temp_directory');
    }

    /**
     * @return string
     */
    public function getJavascriptFilename()
    {
        switch ($this->getBrowser()) {
            case 'casperjs':
                return 'capture.casperjs.js';
            case 'phantomjs';
                return 'capture.phantomjs.js';
        }
    }

    /**
     * @return array
     */
    public function getPaths()
    {
        if (!isset($this->paths)) {
            $this->paths = array();
            foreach ($this->configuration['paths'] as $pathName => $pathConf) {
                $path = new Path($pathName, $pathConf);
                $path->setContainer($this->container);
                $this->paths[] = $path;
            }
        }
        return $this->paths;
    }
}
