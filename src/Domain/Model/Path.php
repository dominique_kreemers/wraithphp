<?php
namespace Domi202\WraithPhp\Domain\Model;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Path
{
    use ContainerAwareTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $pathConfiguration = array();

    /**
     * @param string $name
     * @param array|string $pathConfiguration
     * @param Configuration $configuration
     * @return Path
     */
    public function __construct($name, $pathConfiguration)
    {
        $this->name = $name;
        $this->pathConfiguration = $pathConfiguration;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration()
    {
        return $this->container->get('configuration');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath()
    {
       if (!is_array($this->pathConfiguration)) {
           return $this->pathConfiguration;
       }
       return $this->pathConfiguration['path'];
    }

    /**
     * @return string|null
     */
    public function getSelector()
    {
        if (is_array($this->pathConfiguration) && isset($this->pathConfiguration['selector'])) {
            return $this->pathConfiguration['selector'];
        }
        return null;
    }

    /**
     * @return string|null
     */
    public function getBeforeCapture()
    {
        if (is_array($this->pathConfiguration) && isset($this->pathConfiguration['before_capture'])) {
            return $this->pathConfiguration['before_capture'];
        }
        return null;
    }
}
